package hackzurich.ch.fiftyspend;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Credentials;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;

import hackzurich.ch.fiftyspend.service.ServiceHelper;
import hackzurich.ch.fiftyspend.service.Token;
import hackzurich.ch.fiftyspend.service.UserService;
import hackzurich.ch.fiftyspend.service.UsernamePassword;
import retrofit.Response;

public class LoginActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        final EditText textUsername = (EditText) findViewById(R.id.textUsername);
        final EditText textPassword = (EditText) findViewById(R.id.textPassword);

        Button loginButton = (Button) findViewById(R.id.btn_login);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UsernamePassword userPass =
                        new UsernamePassword(textUsername.getText().toString(), textPassword.getText().toString());

                new LoginAsyncTask().execute(userPass);
            }
        });
    }

    private class LoginAsyncTask extends AsyncTask<UsernamePassword, Void, Token> {

        @Override
        protected Token doInBackground(UsernamePassword... params) {
            UserService userService = ServiceHelper.getService(UserService.class);
            Response<Token> result = null;
            try {
                return userService.login(params[0]).execute().body();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return new Token();
        }

        @Override
        protected void onPostExecute(Token token) {
            SharedPreferences sharedPref = LoginActivity.this.getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("token", token.getToken());
            editor.commit();
        }
    }
}
