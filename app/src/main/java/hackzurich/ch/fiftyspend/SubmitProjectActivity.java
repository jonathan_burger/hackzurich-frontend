package hackzurich.ch.fiftyspend;

import android.app.ExpandableListActivity;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.loopj.android.http.Base64;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import hackzurich.ch.fiftyspend.service.Project;
import hackzurich.ch.fiftyspend.service.ProjectSubmit;
import hackzurich.ch.fiftyspend.service.ProjectsService;
import hackzurich.ch.fiftyspend.service.ServiceHelper;
import retrofit.Response;

public class SubmitProjectActivity extends AppCompatActivity {

    private String encoded;
    private EditText inputTitle;
    private EditText inputDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.submit_projekt);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button selectImage = (Button) findViewById(R.id.btn_select_image);
        selectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent, 1);
            }
        });

        Button takePicture = (Button) findViewById(R.id.btn_take_pic);
        takePicture.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, 2);
                }
            }
        });

        inputTitle = (EditText) findViewById(R.id.input_title);
        inputDescription = (EditText) findViewById(R.id.input_description);

        Button submitButton = (Button) findViewById(R.id.btn_submit);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SubmitProjectAsyncTask().execute(
                        new ProjectSubmit(inputTitle.getText().toString(),
                                inputDescription.getText().toString(),
                                encoded));
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                try {
                    final Uri imageUri = imageReturnedIntent.getData();
                    final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                    Bitmap bitmap = selectedImage;

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
                    findViewById(R.id.btn_select_image).setVisibility(View.GONE);
                    findViewById(R.id.btn_take_pic).setVisibility(View.GONE);
                    findViewById(R.id.second_step).setVisibility(View.VISIBLE);

                }
                catch (Exception e) {
                    System.out.println();
                }
            }
        }
        if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
                try {
                    Bundle extras = imageReturnedIntent.getExtras();
                    Bitmap imageBitmap = (Bitmap) extras.get("data");
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
                    findViewById(R.id.btn_select_image).setVisibility(View.GONE);
                    findViewById(R.id.btn_take_pic).setVisibility(View.GONE);
                    findViewById(R.id.second_step).setVisibility(View.VISIBLE);
                }
                catch (Exception e) {
                    System.out.println(":(");
                }
            }
        }
    }


    private class SubmitProjectAsyncTask extends AsyncTask<ProjectSubmit, Void, Project> {

        @Override
        protected Project doInBackground(ProjectSubmit... params) {
            ProjectsService projectsService = ServiceHelper.getService(ProjectsService.class);

            try {
                return projectsService.addProject(params[0]).execute().body();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Project project) {
            Intent intent = new Intent(SubmitProjectActivity.this, FeedActivity.class);
            SubmitProjectActivity.this.startActivity(intent);
        }
    }
}
