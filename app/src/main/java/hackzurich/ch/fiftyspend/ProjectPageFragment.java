package hackzurich.ch.fiftyspend;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;

import java.io.IOException;

import hackzurich.ch.fiftyspend.service.DonateService;
import hackzurich.ch.fiftyspend.service.DonationResponse;
import hackzurich.ch.fiftyspend.service.Project;
import hackzurich.ch.fiftyspend.service.ProjectDonation;
import hackzurich.ch.fiftyspend.service.ServiceHelper;
import retrofit.Call;
import retrofit.Response;

import static hackzurich.ch.fiftyspend.service.ServiceHelper.getService;

public class ProjectPageFragment extends Fragment {

    private TextView projectTitleView;
    private TextView projectDescriptionView;
    private ImageView projectImageView;
    private TextView projectAuthorView;
    private TextView projectAmountView;
    private TextView projectTimeView;
    private Project project;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.content_feed, container, false);
        projectTitleView = (TextView) rootView.findViewById(R.id.titleView);
        projectDescriptionView = (TextView) rootView.findViewById(R.id.projectDescriptionView);
        projectImageView = (ImageView) rootView.findViewById(R.id.projectImageView);
        projectAuthorView = (TextView) rootView.findViewById(R.id.creatorView);
        projectAmountView = (TextView) rootView.findViewById(R.id.amount);
        projectTimeView = (TextView) rootView.findViewById(R.id.timeView);

        project = (Project) this.getArguments().getSerializable("project");
        if (project == null) return rootView;
        projectTitleView.setText(project.getName());
        projectDescriptionView.setText(project.getDescription());
        projectAuthorView.setText(project.getCreator().getUsername());
        projectAuthorView.setText(project.getCreator().getUsername());
        projectAmountView.setText(parseMoney(project.getAmount()));
        projectTimeView.setText(project.getDate());
        AlphaAnimation animation = new AlphaAnimation(0, 1);
        animation.setDuration(400);
        Ion.with(projectImageView)
                .animateLoad(new AlphaAnimation(0, 1))
                .placeholder(R.drawable.money)
                .load(ServiceHelper.BASE_URL + project.getImage());

        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.btn_donate);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DonateToCurrentProjectTask(view)
                        .execute(project.getId());
            }
        });
        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private String parseMoney(int cents) {
        int aftercomma = cents % 100;
        int fulldollars = (cents - aftercomma)/100;
        String aftercommaleading = (aftercomma < 10) ? "0" + aftercomma : aftercomma+"";
        return "$" + fulldollars + "." + aftercommaleading + " raised";

    }

    private class DonateToCurrentProjectTask extends AsyncTask<String, Void, DonationResponse> {

        private View view;

        public DonateToCurrentProjectTask(View view) {
            this.view = view;
        }

        @Override
        protected DonationResponse doInBackground(String... params) {
            DonateService donationService = getService(DonateService.class);

            Call<DonationResponse> donationServiceCall = donationService.donateToProject(new ProjectDonation(params[0]));

            try {
                Response<DonationResponse> execution = donationServiceCall.execute();
                if (!execution.body().getSuccess()) {
                    Snackbar.make(this.view, "You don't have any money", Snackbar.LENGTH_LONG).show();
                    return null;
                }
                else {
                    return execution.body();
                }
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(DonationResponse response) {
            if (response == null || !response.getSuccess()) {
                Snackbar.make(this.view, "You don't have any money", Snackbar.LENGTH_LONG).show();
                return;
            }
            if (project != null) {
                final int oldAmount = project.getAmount();
                project = response.getProject();
                int newAmount = project.getAmount();
                final int difference = newAmount - oldAmount;
                for (int i = 0; i < 25; i++) {
                    final int _i = i;
                    new android.os.Handler().postDelayed(
                            new Runnable() {
                                public void run() {
                                    projectAmountView.setText(parseMoney(oldAmount + Math.round(Math.round(difference / 25.0f) * (_i + 1))));
                                }
                            },
                            (long)Math.pow(_i, 2)*2);
                }
            }
            else {
                project = response.getProject();
                projectAmountView.setText(parseMoney(project.getAmount()));
            }

            if (ProjectPageFragment.this != null) {
                Animation a = AnimationUtils.loadAnimation(ProjectPageFragment.this.getContext(), R.anim.scale);
                a.reset();
                projectAmountView.clearAnimation();
                projectAmountView.startAnimation(a);
            }


            Snackbar.make(view, response != null ? "Thanks for your donation!!" : "Donating wasn't successful!", Snackbar.LENGTH_LONG).show();
        }
    }
}
