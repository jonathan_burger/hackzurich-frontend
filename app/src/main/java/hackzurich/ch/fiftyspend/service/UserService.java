package hackzurich.ch.fiftyspend.service;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.POST;

public interface UserService {

    @POST("user/login")
    Call<Token> login(@Body UsernamePassword credentials);

}
