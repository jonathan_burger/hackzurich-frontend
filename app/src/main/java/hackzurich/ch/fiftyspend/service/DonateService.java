package hackzurich.ch.fiftyspend.service;

import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.POST;

public interface DonateService {

    @POST("donations")
    Call<DonationResponse> donateToProject(@Body ProjectDonation donation);

}
