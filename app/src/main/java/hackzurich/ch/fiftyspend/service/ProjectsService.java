package hackzurich.ch.fiftyspend.service;

import java.util.List;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;

public interface ProjectsService {

    @GET("projects")
    Call<List<Project>> getProjects();

    @POST("projects")
    Call<Project> addProject(@Body ProjectSubmit project);
}
