package hackzurich.ch.fiftyspend.service;

import retrofit.MoshiConverterFactory;
import retrofit.Retrofit;

public class ServiceHelper {
    public static final String BASE_URL = "http://crowdstartr.mod.bz/api/"; // remote
//     public static final String BASE_URL = "http://10.0.2.2:9000/api/"; // local

    public static <T> T getService(Class<? extends T> serviceClass) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .build();

        return retrofit.create(serviceClass);
    }
}
