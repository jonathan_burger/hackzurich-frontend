package hackzurich.ch.fiftyspend.service;

public class DonationResponse {
    private int newCredit;
    private Project project;
    private boolean success;

    public int getNewCredit() {
        return newCredit;
    }

    public void setNewCredit(int newCredit) {
        this.newCredit = newCredit;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public boolean getSuccess() { return success;}
    public void setSuccess(boolean success) {
        this.success = success;
    }


}
