package hackzurich.ch.fiftyspend.service;

public class ProjectSubmit {
    private String name;
    private String description;
    private String image;

    public ProjectSubmit(String name, String description, String image) {
        this.name = name;
        this.description = description;
        this.image = image;
    }
}
