package hackzurich.ch.fiftyspend.service;

import java.io.Serializable;

/**
 * Created by jonnyburger on 03.10.15.
 */
public class User implements Serializable{
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private String username;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;
}
