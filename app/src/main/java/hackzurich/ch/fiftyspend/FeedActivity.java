package hackzurich.ch.fiftyspend;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import hackzurich.ch.fiftyspend.service.ProjectsService;
import hackzurich.ch.fiftyspend.service.Project;

import static hackzurich.ch.fiftyspend.service.ServiceHelper.*;

public class FeedActivity extends AppCompatActivity {

    private ViewPager pager;
    private ProjectPagerAdapter pagerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (TextUtils.isEmpty(getSharedPreferences("token", 0).getString("token", null))) {
//            Intent intent = new Intent(this, LoginActivity.class);
//            startActivity(intent);
//        }

        setContentView(R.layout.activity_feed);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        pager = (ViewPager) findViewById(R.id.pager);
        pagerAdapter = new ProjectPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);


        new RetrieveFeedTask().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (pager.getCurrentItem() == 0) {
            super.onBackPressed();
        } else {
            pager.setCurrentItem(pager.getCurrentItem() - 1);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.submit_project) {
            Intent intent = new Intent(this, SubmitProjectActivity.class);
            startActivity(intent);
        }
        if (id == R.id.refresh) {
            new RetrieveFeedTask().execute();
        }
        return super.onOptionsItemSelected(item);
    }




    private class ProjectPagerAdapter extends FragmentStatePagerAdapter {

        private List<Project> projects = Collections.emptyList();

        public ProjectPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            ProjectPageFragment fragment = new ProjectPageFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("project", getProjectByPosition(position));
            fragment.setArguments(bundle);
            return fragment;
        }

        @Override
        public int getCount() {
            return projects != null ? projects.size() : 0;
        }

        public void setProjects(List<Project> projects) {
            this.projects = projects;
        }

        public List<Project> getProjects() {
            return projects;
        }

        public Project getProjectByPosition(int position) {
            return projects.get(position);
        }
    }

    private class RetrieveFeedTask extends AsyncTask<Void, Void, List<Project>> {

        @Override
        protected List<Project> doInBackground(Void... params) {
            ProjectsService service = getService(ProjectsService.class);
            try {
                return service.getProjects().execute().body();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Project> projects) {
            pagerAdapter.setProjects(projects);
            pagerAdapter.notifyDataSetChanged();
            pager.setAdapter(pagerAdapter);
        }
    }
}
